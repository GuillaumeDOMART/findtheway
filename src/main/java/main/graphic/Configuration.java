package main.graphic;

import com.jme3.system.AppSettings;
import main.Game;

import java.awt.*;

public class Configuration {

    public static Game configure(Game game) {
        System.setProperty("java.awt.headless", "false");
        AppSettings settings = new AppSettings(true);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize() ;
        settings.setResolution(screenSize.width, screenSize.height);
        settings.setFullscreen(true);

        settings.setTitle("FindTheWay");

        game.setShowSettings(false);
        game.setSettings(settings);
        return game;
    }

}
