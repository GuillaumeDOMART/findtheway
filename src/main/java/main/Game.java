package main;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import main.graphic.Configuration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Game extends SimpleApplication implements CommandLineRunner {

    public void run(String[] args) {
//        System.out.println("Lancement de la génération du labyrinthe");
//        Maze maze = new BasicMazeFactory(new Size(10, 10)).process(Generator.FUSION_RANG);
//        System.out.println("Labyrinthe généré\n");
//        System.out.println(maze);

        Game game = Configuration.configure(new Game());
        game.start();

    }

    @Override
    public void simpleInitApp() {
        Quad quad = new Quad(2, 2);
        Geometry geom = new Geometry("Box", quad);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        geom.setMaterial(mat);
        rootNode.attachChild(geom);
    }
}
