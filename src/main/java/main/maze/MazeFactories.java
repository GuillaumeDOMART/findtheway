package main.maze;


import main.maze.generator.Generator;

public interface MazeFactories {
    Maze process(Generator generator);
}
