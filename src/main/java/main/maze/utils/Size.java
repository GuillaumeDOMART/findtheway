package main.maze.utils;

public record Size(Integer width, Integer height) {
}
