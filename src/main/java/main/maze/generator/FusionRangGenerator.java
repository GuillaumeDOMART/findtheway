package main.maze.generator;

import main.maze.components.Box;
import main.maze.components.FusionRangBox;
import main.maze.components.Junction;
import main.maze.utils.Coordinate;
import main.maze.utils.Size;

import java.util.Objects;
import java.util.Random;

public class FusionRangGenerator implements MazeGenerator {
    private double classes;
    private final Size size;
    private final FusionRangBox[] map;

    private final Coordinate exit;

    public FusionRangGenerator(Size size) {
        this.classes = size.width() * size.height();
        this.size = size;
        this.map = new FusionRangBox[size.height() * size.width()];
        this.exit = new Coordinate(size.width() - 1, size.height() - 1);
    }

    @Override
    public MazeGenerator generateMaze() {
        for (int i = 0; i < map.length; i++)
            map[i] = new FusionRangBox(Coordinate.getCoordinate(i, size));
        map[0].setPresence(1);

//        for (int i = 0; i < map.length; i++) {
//            System.out.println(Coordinate.getCoordinate(i, size));
//        }

        return this;
    }

    private Coordinate findAncestor(Coordinate coordinate) {
        FusionRangBox position = map[coordinate.getPosition(size)];
        if (Objects.equals(position.getPere().x(), coordinate.x()) && Objects.equals(position.getPere().y(), coordinate.y()))
            return position.getPere();

        var racine = findAncestor(position.getPere());
        position.setPere(racine);

        return racine;
    }

    private void fusionRang(Junction junction) {
        Coordinate firstRacine = findAncestor(junction.firstBox());
        Coordinate secondRacine = findAncestor(junction.secondBox());

        if ((!Objects.equals(firstRacine.x(), secondRacine.x())) || (!Objects.equals(firstRacine.y(), secondRacine.y()))) {
            if (map[firstRacine.getPosition(size)].getRang() < map[secondRacine.getPosition(size)].getRang())
                map[firstRacine.getPosition(size)].setPere(secondRacine);
            else if (map[firstRacine.getPosition(size)].getRang() > map[secondRacine.getPosition(size)].getRang())
                map[secondRacine.getPosition(size)].setPere(firstRacine);
            else {
                map[firstRacine.getPosition(size)].setPere(secondRacine);
                map[secondRacine.getPosition(size)].setRang(map[secondRacine.getPosition(size)].getRang() + 1);
            }
        }
    }

    private Integer breakWall(Junction junction) {
        Coordinate firstBox = junction.firstBox();
        Coordinate secondBox = junction.secondBox();
        Coordinate firstRacine = findAncestor(firstBox);
        Coordinate secondRacine = findAncestor(secondBox);

        if (!Objects.equals(firstRacine.x(), secondRacine.x()) || !Objects.equals(firstRacine.y(), secondRacine.y())) {
            classes--;
            fusionRang(junction);
            if (Objects.equals(firstBox.y(), secondBox.y())) {
                if (firstBox.x() > secondBox.x())
                    map[secondBox.getPosition(size)].setMurEst(0);
                else
                    map[firstBox.getPosition(size)].setMurEst(0);
            } else if (Objects.equals(firstBox.x(), secondBox.x())) {
                if (firstBox.y() > secondBox.y())
                    map[secondBox.getPosition(size)].setMurSud(0);
                else
                    map[firstBox.getPosition(size)].setMurSud(0);
            }
            return 1;
        }
        return 0;
    }

    private Junction boxDraw(Random random) {
        while (true) {
            Coordinate coordinate = new Coordinate(
                    random.nextInt(size.width()),
                    random.nextInt(size.height())
            );
            int wall = random.nextInt(2);

            if (wall == 0 && coordinate.x() + 1 < size.width() && map[coordinate.getPosition(size)].getMurEst() == 1)
                return new Junction(
                        coordinate,
                        new Coordinate(coordinate.x() + 1, coordinate.y())
                );
            if (wall == 1 && coordinate.y() + 1 < size.height() && map[coordinate.getPosition(size)].getMurSud() == 1)
                return new Junction(
                        coordinate,
                        new Coordinate(coordinate.x(), coordinate.y() + 1)
                );
        }
    }

    private Integer sameRoot(Junction junction) {
        Coordinate coordinate1 = findAncestor(junction.firstBox());
        Coordinate coordinate2 = findAncestor(junction.secondBox());

        if ((Objects.equals(coordinate1.x(), coordinate2.x())) && (Objects.equals(coordinate1.y(), coordinate2.y()))) {
            return 1;
        }
        return 0;
    }

    @Override
    public MazeGenerator prepareMaze(Random random) {
        Junction junction = boxDraw(random);

        while (true) {
            while (breakWall(junction) != 1) {
                junction = boxDraw(random);
            }

            if (sameRoot(new Junction(new Coordinate(0, 0), exit)) == 1) {
                if (classes == 1)
                    return this;
            }
        }
    }

    @Override
    public Box[] getMazeMap() {
        return map;
    }

    @Override
    public Size getMazeSize() {
        return size;
    }

    private Box getLeftUp(int indexCoordinate) {
        if (indexCoordinate % size.width() == 0 || indexCoordinate < size.width())
            return null;
        return map[indexCoordinate - size.width() - 1];
    }

    private Box getUp(int indexCoordinate) {
        if (indexCoordinate < size.width())
            return null;
        return map[indexCoordinate - size.width()];
    }

    private Box getRightUp(int indexCoordinate) {
        if (indexCoordinate % size.width() == size.width() - 1 || indexCoordinate < size.width())
            return null;
        return map[indexCoordinate - size.width() + 1];
    }

    private Box getLeft(int indexCoordinate) {
        if (indexCoordinate % size.width() == 0)
            return null;
        return map[indexCoordinate - 1];
    }

    private Box getRight(int indexCoordinate) {
        if (indexCoordinate % size.width() == size.width() - 1)
            return null;
        return map[indexCoordinate + 1];
    }

    private Box getLeftDown(int indexCoordinate) {
        if (indexCoordinate % size.width() == 0 || indexCoordinate >= size.width() * (size.height() - 1))
            return null;
        return map[indexCoordinate + size.width() - 1];
    }

    private Box getDown(int indexCoordinate) {
        if (indexCoordinate >= size.width() * (size.height() - 1))
            return null;
        return map[indexCoordinate + size.width()];
    }

    private Box getRightDown(int indexCoordinate) {
        if (indexCoordinate % size.width() == size.width() * size.height() - 1 || indexCoordinate >= size.width() * (size.height() - 1))
            return null;
        return map[indexCoordinate + size.width() + 1];
    }

    @Override
    public String toString() {
        StringBuilder horizontalStringBuilder = new StringBuilder();
        StringBuilder verticalStringBuilder = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();


//        System.out.println("╹" + "╻" + "╸" + "╺");
//        4 x ━
//      "┏━━━━┳━━━━┓"
//      "┃    ┃    ┃"
//      "┣━━━━╋━━━━┫"
//      "┃    ┃    ┃"
//      "┗━━━━┻━━━━┛"

        for (int i = 0; i < size.width(); i++) {
            if (i == 0)
                horizontalStringBuilder.append("┏");
            horizontalStringBuilder.append("━━━━");
            if (getRight(i) == null)
                horizontalStringBuilder.append("┓");
            else {
                if (map[i].getMurEst() == 1)
                    horizontalStringBuilder.append("┳");
                else
                    horizontalStringBuilder.append("━");
            }
        }
        stringBuilder.append(horizontalStringBuilder.append("\n"));
        horizontalStringBuilder.delete(0, stringBuilder.length());

        for (int i = 0; i < map.length; i++) {
            if (i % size.width() == 0) {
                verticalStringBuilder.append("┃");
                if (i >= size.width() * (size.height() - 1))
                    horizontalStringBuilder.append("┗");
                else {
                    if (map[i].getMurSud() == 1)
                        horizontalStringBuilder.append("┣");
                    else
                        horizontalStringBuilder.append("┃");
                }
            }
            verticalStringBuilder.append("    ");
            if (map[i].getMurEst() == 1)
                verticalStringBuilder.append("┃"); 
            else
                verticalStringBuilder.append(" ");
            if (map[i].getMurSud() == 1)
                horizontalStringBuilder.append("━━━━");
            else
                horizontalStringBuilder.append("    ");
            horizontalStringBuilder.append(" ");


//            stringBuilder.append("Mur sud : ").append(map[i].getMurSud()).append("Mur est : ").append(map[i].getMurEst()).append("\n");
            if (i % size.width() == size.width() - 1) {
                stringBuilder.append(verticalStringBuilder.append("\n"));
                stringBuilder.append(horizontalStringBuilder.append("\n"));
                verticalStringBuilder.delete(0, verticalStringBuilder.length());
                horizontalStringBuilder.delete(0, horizontalStringBuilder.length());
            }
        }

        return stringBuilder.toString();
    }
}
