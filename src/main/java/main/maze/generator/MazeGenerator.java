package main.maze.generator;

import main.maze.components.Box;
import main.maze.utils.Size;

import java.util.Random;

public interface MazeGenerator {
    MazeGenerator generateMaze();
    MazeGenerator prepareMaze(Random random);
    Box[] getMazeMap();
    Size getMazeSize();
    String toString();

}
