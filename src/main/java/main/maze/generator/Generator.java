package main.maze.generator;

import main.maze.utils.Size;

public enum Generator {
    FUSION_RANG;

    public MazeGenerator getGenerator(Size size) throws IllegalAccessException {
        switch (this) {
            case FUSION_RANG -> {
                return new FusionRangGenerator(size);
            }
            default -> throw new IllegalAccessException();
        }
    }
}
