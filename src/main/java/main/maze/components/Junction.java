package main.maze.components;

import main.maze.utils.Coordinate;

public record Junction(Coordinate firstBox, Coordinate secondBox) {
}
